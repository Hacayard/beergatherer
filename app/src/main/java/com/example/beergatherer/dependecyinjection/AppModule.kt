package com.example.beergatherer.dependecyinjection

import com.example.beergatherer.constants.Constants.BASE_URL
import com.example.beergatherer.data.api.BeerApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun providesRetrofit() :Retrofit{
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Singleton
    @Provides
    fun providesBeerApi(retrofit: Retrofit) : BeerApi
    {
        return retrofit.create(BeerApi::class.java)
    }
}