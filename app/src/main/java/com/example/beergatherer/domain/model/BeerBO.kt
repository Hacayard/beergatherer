package com.example.beergatherer.domain.model

data class BeerBO(
    val id : Int,
    val name : String,
    val description : String,
    val imageUrl : String?,
    val abv : Float
)