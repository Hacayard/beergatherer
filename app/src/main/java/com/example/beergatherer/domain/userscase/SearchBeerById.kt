package com.example.beergatherer.domain.userscase

import com.example.beergatherer.data.repository.BeersRepository
import com.example.beergatherer.domain.mapper.toBO
import com.example.beergatherer.domain.model.BeerBO
import javax.inject.Inject

class SearchBeerById @Inject constructor(private val beersRepository: BeersRepository){

    suspend fun getBeerById(id : Int) : BeerBO?
    {
        return beersRepository.getBeerById(id)?.toBO()?.get(0) ?: null
    }
}