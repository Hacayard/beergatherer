package com.example.beergatherer.domain.mapper

import com.example.beergatherer.data.model.BeerDTO
import com.example.beergatherer.domain.model.BeerBO

fun BeerDTO.toBO() : BeerBO
{
    return BeerBO(
        id = this.id,
        name= this.name,
        description = this.description,
        imageUrl = this.image_url,
        abv = this.abv
    )
}

fun List<BeerDTO>.toBO() : List<BeerBO>
{
    return this.map { item ->  item.toBO() }
}

fun Array<BeerDTO>.toBO() : Array<BeerBO>
{
    return  this.map { item -> item.toBO() }.toTypedArray()
}