package com.example.beergatherer.domain.userscase

import com.example.beergatherer.data.repository.BeersRepository
import com.example.beergatherer.domain.mapper.toBO
import com.example.beergatherer.domain.model.BeerBO
import javax.inject.Inject


class SearchBeerByNameUserCase @Inject constructor(private val beersRepository: BeersRepository){

    suspend fun getBeersByName(name : String) : List<BeerBO>?
    {
        return beersRepository.getBeerByName(name)?.toBO()
    }
}