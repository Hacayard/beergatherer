package com.example.beergatherer.constants

object Constants {
    const val BASE_URL = "https://api.punkapi.com/v2/ "
    const val ENDPOINT = "beers"
    const val BEER_NAMEKEY = "?beer_name="
}