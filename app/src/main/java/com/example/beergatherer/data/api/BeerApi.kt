package com.example.beergatherer.data.api

import com.example.beergatherer.constants.Constants.BEER_NAMEKEY
import com.example.beergatherer.constants.Constants.ENDPOINT
import com.example.beergatherer.data.model.BeerDTO
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface BeerApi {
    @GET("$ENDPOINT")
    suspend fun getBeersByName(@Query(value = "beer_name") beerName : String) : Response<List<BeerDTO>>

    @GET("$ENDPOINT/{id}")
    suspend fun getBeerByID(@Path(value = "id") id : Int) : Response<Array<BeerDTO>>
}