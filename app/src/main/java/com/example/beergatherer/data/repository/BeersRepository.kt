package com.example.beergatherer.data.repository

import android.util.Log
import com.example.beergatherer.data.api.BeerApi
import com.example.beergatherer.data.model.BeerDTO
import com.example.beergatherer.domain.mapper.toBO
import com.example.beergatherer.domain.model.BeerBO
import javax.inject.Inject

class BeersRepository @Inject constructor(private val beerApi : BeerApi) {

    suspend fun getBeerByName(name : String) : List<BeerDTO>? {
        val response = beerApi.getBeersByName(name)
        if(response.isSuccessful)
        {
            return response.body()
        }
        return null
    }

    suspend fun getBeerById(id: Int) : Array<BeerDTO>?
    {
        val response = beerApi.getBeerByID(id)
        if(response.isSuccessful)
        {
            return response.body()
        }
        return  null
    }
}