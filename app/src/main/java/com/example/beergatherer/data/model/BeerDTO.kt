package com.example.beergatherer.data.model

data class BeerDTO(
    val id : Int,
    val name : String,
    val description : String,
    val image_url : String?,
    val abv : Float
)
