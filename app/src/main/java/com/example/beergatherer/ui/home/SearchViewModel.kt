package com.example.beergatherer.ui.home

import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.beergatherer.domain.model.BeerBO
import com.example.beergatherer.data.repository.BeersRepository
import com.example.beergatherer.domain.userscase.SearchBeerByNameUserCase
import com.example.beergatherer.ui.detailed.BeerState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(private val userCase: SearchBeerByNameUserCase) : ViewModel() {

    private val _searchText = MutableStateFlow("")
    val searchText = _searchText.asStateFlow()

    private val _beers = MutableStateFlow<List<BeerBO>>(emptyList())
    val beers = _beers.asStateFlow()

    var showAlert by mutableStateOf(false)

    val beersFiltered = searchText
        .combine(_beers){ text,beers ->
            if(text.isBlank())
            {
                _beers.value = emptyList()
            }
            beers.filter { beer ->
                beer.name.uppercase().contains(text.trim().uppercase())
            }
        }.stateIn(
            scope = viewModelScope,
            started = SharingStarted.WhileSubscribed(5000),
            initialValue = _beers.value
        )

    private fun fetchBeersByName(name : String)
    {

        viewModelScope.launch {
            withContext(Dispatchers.IO)
            {
                try {
                    delay(150)
                    val result = userCase.getBeersByName(name)
                    _beers.value = result ?: emptyList()
                }
                catch (e : Exception)
                {
                    showAlert = true
                }
            }
        }

    }

    fun clearSearchText()
    {
        _searchText.value = ""
    }

    fun onSearchChange( newQuery : String)
    {
        _searchText.value = newQuery
        if(_searchText.value.isNotEmpty())
        {
            fetchBeersByName(_searchText.value)
        }
    }

    fun dismissAlert()
    {
        showAlert = false
    }
}