package com.example.beergatherer.ui.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import com.example.beergatherer.R


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun BeerCardComponent(beerName : String, onClickEvent: () -> Unit)
{
    Card(
        onClick = { onClickEvent ()},
        colors = CardDefaults.cardColors(
            containerColor = MaterialTheme.colorScheme.secondaryContainer,
            contentColor = MaterialTheme.colorScheme.secondary
        ),
        shape = RoundedCornerShape(dimensionResource(R.dimen.standardBorder)),
    ) {
        Row(
            horizontalArrangement = Arrangement.SpaceEvenly,
            modifier = Modifier
                .fillMaxSize()
        ) {
            Text(
                text = beerName,
                fontWeight = FontWeight.Bold,
                modifier = Modifier
                    .weight(2f)
                    .padding(start =dimensionResource(R.dimen.mediumPadding),
                        top = dimensionResource(R.dimen.mediumPadding),
                        bottom = dimensionResource(R.dimen.mediumPadding))
            )
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .weight(0.4f)
                    .padding(end = dimensionResource(R.dimen.mediumPadding),
                        top = dimensionResource(R.dimen.mediumPadding),
                        bottom = dimensionResource(R.dimen.mediumPadding)),
                contentAlignment = Alignment.Center,
            )
            {
                Icon(
                    imageVector = Icons.Default.ArrowForward,
                    contentDescription = "",

                )
            }
        }
    }
}