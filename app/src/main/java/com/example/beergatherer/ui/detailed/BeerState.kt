package com.example.beergatherer.ui.detailed

data class BeerState(
    val id : Int = 0,
    val name : String = "",
    val description : String = "",
    val image_url : String ="",
    val abv : Float = 0f
)
