package com.example.beergatherer.ui.detailed

import android.annotation.SuppressLint
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.example.beergatherer.R
import com.example.beergatherer.ui.components.Alert
import com.example.beergatherer.ui.home.SearchViewModel

@OptIn(ExperimentalMaterial3Api::class)
@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun DetailedScreen(navController: NavController, detailedViewModel: DetailedViewModel, id : Int)
{

    LaunchedEffect(Unit)
    {
        detailedViewModel.getBeerByID(id)
    }

    DisposableEffect(Unit)
    {
        onDispose {
            detailedViewModel.clearState()
        }
    }

    val beerState = detailedViewModel.beerState

    Scaffold (
        topBar = {
                 TopAppBar(
                     title = { Text(text =  beerState.name)},
                     colors = TopAppBarDefaults.mediumTopAppBarColors(
                         containerColor = MaterialTheme.colorScheme.tertiaryContainer,
                         titleContentColor = MaterialTheme.colorScheme.tertiary
                     ),
                     navigationIcon = {
                         IconButton(onClick = { navController.popBackStack()}) {
                             Icon(imageVector = Icons.Default.ArrowBack, contentDescription = "")
                         }
                     }
                 )
        },
        contentColor = MaterialTheme.colorScheme.primary,
        modifier = Modifier
            .fillMaxSize()

    ){
        Column(
            modifier = Modifier
                .padding(paddingValues = it)
                .padding(vertical = dimensionResource(R.dimen.bigPadding),)
                .verticalScroll(rememberScrollState()),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment =  Alignment.CenterHorizontally
        ) {
            AsyncImage(
                model = ImageRequest.Builder(LocalContext.current)
                    .data(beerState.image_url)
                    .crossfade(true)
                    .build(),
                placeholder = painterResource( R.drawable.ic_launcher_foreground),
                contentDescription =""
                )
            Spacer(modifier = Modifier.height(dimensionResource(R.dimen.mediumPadding)))
            Row(
                horizontalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier
                    .height(IntrinsicSize.Min)
                    .padding(
                        start = dimensionResource(R.dimen.standardPadding),
                        end = dimensionResource(R.dimen.standardPadding)
                    )
            ){
                Box(
                    modifier = Modifier
                        .padding(
                            horizontal = dimensionResource(R.dimen.smallPadding),
                            vertical = dimensionResource(R.dimen.smallPadding)
                        )
                        .fillMaxHeight()
                )
                {
                    Row(
                        modifier = Modifier
                            .fillMaxSize(),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Box(modifier = Modifier
                            .fillMaxSize()
                            .weight(1.7f)
                            .border(
                                dimensionResource(R.dimen.smallBorder),
                                MaterialTheme.colorScheme.secondary,
                                RoundedCornerShape(dimensionResource(R.dimen.mediumBorder))
                            ),
                            contentAlignment = Alignment.Center

                        )
                        {
                            Text(
                                text = beerState.description,
                                modifier = Modifier
                                    .padding(dimensionResource(R.dimen.smallPadding))
                            )
                        }
                        Box(modifier = Modifier
                            .fillMaxSize()
                            .weight(0.3f)
                            .border(
                                dimensionResource(R.dimen.smallBorder),
                                MaterialTheme.colorScheme.secondary,
                                RoundedCornerShape(dimensionResource(R.dimen.mediumBorder))
                            ),
                            contentAlignment = Alignment.Center
                            )
                        {
                            Text(
                                text = beerState.abv.toString() + "º",
                                modifier = Modifier
                                    .padding(dimensionResource(R.dimen.smallPadding))
                            )
                        }
                    }
                }
            }

            if(detailedViewModel.showAlert)
            {
                Alert(title = "Alerta", message = "Su cerveza se a ha perdido. Compruebe que todo esta bien con su conexión a internet", confirmText ="Ok" , onConfirmClick = { detailedViewModel.dismissAlert()}) {}
            }
        }
    }
}