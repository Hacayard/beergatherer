package com.example.beergatherer.ui.home

import android.annotation.SuppressLint
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SearchBar
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.example.beergatherer.R
import com.example.beergatherer.ui.components.Alert
import com.example.beergatherer.ui.components.BeerCardComponent

@OptIn(ExperimentalMaterial3Api::class)
@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun HomeScreen(navController: NavController, searchViewModel: SearchViewModel)
{

    val searchText by searchViewModel.searchText.collectAsState()
    val beerList by searchViewModel.beersFiltered.collectAsState()

    Scaffold (
        topBar = {
            TopAppBar(
                title = {
                    Text(
                        text = stringResource(id = R.string.HomeTitle),
                        fontWeight = FontWeight.Bold
                    )},
                colors = TopAppBarDefaults.mediumTopAppBarColors(
                    containerColor = MaterialTheme.colorScheme.tertiaryContainer,
                    titleContentColor = MaterialTheme.colorScheme.tertiary
                )
                )
        },
        contentColor = MaterialTheme.colorScheme.primary,
        modifier = Modifier
            .fillMaxSize()
    ){
        Column (
            modifier = Modifier
                .padding(it)
                .padding(horizontal = dimensionResource(R.dimen.standardPadding))
                .fillMaxWidth()

        ){
            OutlinedTextField(
                value = searchText,
                onValueChange = {searchViewModel.onSearchChange(it)},
                leadingIcon = {
                    Icon(imageVector = Icons.Default.Search, contentDescription = "")
                },
                trailingIcon = {
                   IconButton(onClick = { searchViewModel.clearSearchText()}) {
                       Icon(imageVector = Icons.Default.Clear, contentDescription ="")
                   }
                },
                modifier = Modifier
                    .fillMaxWidth()
                    .clip(RoundedCornerShape(dimensionResource(R.dimen.standardBorder)))
                    .padding(top = dimensionResource(R.dimen.standardPadding))
            )

            LazyColumn(
                modifier = Modifier
                    .fillMaxSize()
                    .clip(RoundedCornerShape(dimensionResource(R.dimen.standardBorder)))
                    .padding(vertical = dimensionResource(R.dimen.standardPadding))
            )
            {
                items(beerList) { beer ->
                    BeerCardComponent(beerName = beer.name) {
                        navController.navigate("DetailedView/${beer.id}")
                    }
                }
            }
        }
        if(searchViewModel.showAlert)
        {
            Alert(title = "Alerta", message = "No podemos buscar su cerveza. Compruebe que tienes acceso a internet para reanudar la busqueda", confirmText ="Ok" , onConfirmClick = { searchViewModel.dismissAlert()}) {}
        }
    }
}