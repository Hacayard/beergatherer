package com.example.beergatherer.ui.detailed

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.beergatherer.domain.userscase.SearchBeerById
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class DetailedViewModel @Inject constructor(private val userCase: SearchBeerById) : ViewModel() {

    var beerState by mutableStateOf(BeerState())
        private set
    var showAlert by mutableStateOf(false)

    fun getBeerByID(id : Int)
    {
        viewModelScope.launch {
            try {
                withContext(Dispatchers.IO)
                {
                    val result = userCase.getBeerById(id)
                    if (result != null) {
                        beerState = beerState.copy(
                            name = result.name ?: "",
                            description = result.description ?: "",
                            image_url = result.imageUrl ?: "",
                            abv = result.abv ?: 0f
                        )
                    }
                }
            }catch (e : Exception)
            {
                showAlert = true
            }
        }
    }

    fun clearState()
    {
        beerState = beerState.copy(
            name = "",
            description ="",
            image_url =  "",
            abv = 0f
        )
    }

    fun dismissAlert()
    {
        showAlert = false
    }

}