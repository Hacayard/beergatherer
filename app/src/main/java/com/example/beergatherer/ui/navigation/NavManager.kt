package com.example.beergatherer.ui.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.beergatherer.ui.home.SearchViewModel
import com.example.beergatherer.ui.detailed.DetailedScreen
import com.example.beergatherer.ui.detailed.DetailedViewModel
import com.example.beergatherer.ui.home.HomeScreen

@Composable
fun NavManager(searchViewModel : SearchViewModel, detailedViewModel: DetailedViewModel)
{
    val navController = rememberNavController()
    NavHost(navController, startDestination = "HomeView" ){

        composable("HomeView"){
            HomeScreen(navController,searchViewModel)
        }

        composable("DetailedView/{id}", arguments = listOf(
            navArgument("id"){type = NavType.IntType}
        )){
            val id = it.arguments?.getInt("id") ?: 0
            DetailedScreen(navController,detailedViewModel,id)
        }
    }
}