package com.example.beergatherer

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import com.example.beergatherer.ui.detailed.DetailedViewModel
import com.example.beergatherer.ui.navigation.NavManager
import com.example.beergatherer.ui.home.SearchViewModel
import com.example.compose.BeerGathererTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val searchViewModel : SearchViewModel by viewModels()
        val detailedViewModel : DetailedViewModel by viewModels()
        setContent {
            BeerGathererTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    NavManager(searchViewModel,detailedViewModel)
                }
            }
        }
    }
}